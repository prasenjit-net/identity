# identity
A OAuth 2 authorization server implementation conforming to rfc6749

## Implemented Specifications

https://tools.ietf.org/html/rfc6749
https://tools.ietf.org/html/rfc6750
https://tools.ietf.org/html/rfc6819
https://tools.ietf.org/html/rfc7009
https://tools.ietf.org/html/rfc7662
https://tools.ietf.org/html/rfc7636
https://tools.ietf.org/html/rfc7519
https://tools.ietf.org/html/rfc7591
https://tools.ietf.org/html/rfc7592
https://tools.ietf.org/html/rfc7521